const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const axios = require('axios');
// const {decryptSurveyCake} = require("../service/decrypt-surveycake-data");
require('dotenv').config();

const app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(morgan(':date[iso] - :method :url :status - :response-time ms'));


const port = process.env.PORT || 3000;
const surveycakeDomain = process.env.SURVEYCAKE_DOMAIN
const hashKey = process.env.HASH_KEY;
const ivKey = process.env.IV_KEY;
const googleSheetWebhook = process.env.GOOGLE_SHEET_WEBHOOK
const super8Webhook = process.env.SUPER8_WEBHOOK


app.get('/', (req, res) => {
    res.send('Alive!  surveycakeDomain: ' + surveycakeDomain + '\n')
    console.log('surveycakeDomain: ', surveycakeDomain);
    console.log('hashKey: ', hashKey);
    console.log('ivKey: ', ivKey);
});

// receive from surveycake
app.post('/webhook', async (req, res) => {
    const {body} = req;
    console.log('webhook received: ', body);

    try {
        await Promise.all([
            sendGoogleSheet(body, googleSheetWebhook),
            sendSuper8(body)
        ]);
    } catch (e) {
        console.error('webhook process error: ', e);
        res.status(500).send('webhook process error');
    }

    res.status(200).send('webhook process success');
});

async function sendGoogleSheet(body, googleSheetWebhook) {
    let params = new URLSearchParams(body);
    const result = await axios.post(`${googleSheetWebhook}?${params}`);
    console.log('send to google sheet webhook: ', result.data);
}

async function sendSuper8(body) {
    const result = await axios.post(super8Webhook, body);
    console.log('send to super8 webhook: ', result.data);
}

app.listen(port, () => {
    console.log(`server is running on port ${port}`);
});

module.exports = app;
