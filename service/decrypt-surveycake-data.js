const crypto = require("crypto");
const axios = require("axios");

async function decryptSurveyCake(body, surveycakeDomain, hashKey, ivKey) {
  // 取得加密問卷內容
  return await axios
    .get(`${surveycakeDomain}/webhook/v0/${body.svid}/${body.hash}`)
    .then((response) => decrypt(hashKey, ivKey, response.data))
    .catch((error) => {
      console.log("error: ", error);
    });
}

function decrypt(hashKey, ivKey, data) {
  // decrypt
  let decipher = crypto.createDecipheriv("aes-128-cbc", hashKey, ivKey);
  let json = decipher.update(data, "base64", "utf8");
  json += decipher.final("utf8");
  //   console.log("survey data: ", json);

  const result = JSON.parse(json)
    .result.sort((a, b) => a.sn - b.sn)
    .reduce((obj, item) => {
      obj[item.subject] = item.answer.length > 1 ? item.answer : item.answer[0];
      return obj;
    }, {});
  // .map((item) => {
  //   return {
  //     key: item.subject,
  //     value: item.answer,
  //   };
  // });
  return result;
}

module.exports = { decryptSurveyCake };
