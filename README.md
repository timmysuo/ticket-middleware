
## Summary
將 SurveyCake 的問卷結果存入 GoogleSheet，並發送 Line(Super8) 通知

## Prepare
* SurveyCake
    1. 註冊/登入帳號
    2. 建立問卷
    3. 根據不同需求新增欄位(optional)
    4. 新增 s8_uid, s8_uid_raw 兩個欄位，必須分別設定別名，一樣 s8_uid, s8_uid_raw (required)


## Flow

### SurveyCake
1. 註冊/登入帳號
2. 建立問卷
3. 根據不同需求新增欄位(optional)
4. 新增 s8_uid, s8_uid_raw 兩個欄位，必須分別設定別名，一樣 s8_uid, s8_uid_raw (required)
5. 記下問卷資訊(url, HashKey, IVKey)
6. 填寫 express server webhook url
![alt text](readme/image-3.png)

### Super8
1. 註冊/登入帳號
2. 應用市集->SurveyCake問卷->前往編輯->新增 SurveyCake串接
3. 填寫 SurveyCake 問卷資訊
    ![alt text](readme/image.png)
4. 因為 Super8 需要獲取 line用戶的 uid 訊息，所以需要用此功能建立自帶 uid 參數，群發訊息->新增群發訊息->指定對象->計算符合條件的客戶數->圖文訊息
![alt text](readme/image-2.png)
5. 選擇url，輸入 SurveyCake 問卷 url
![alt text](readme/image-1.png)
6. 訊息中心->確認有寄出圖文連結通知

### GoogleSheet
1. 註冊/登入帳號
2. 建立表單->擴充功能->App Scripts
3. 參考[SurveyCake 串接 GoogleSheet](https://blog.SurveyCake.com/features/pro/survey-google-sheets/)，app script 會處理後續加密資料
4. 部署參考![alt text](readme/image-5.png)
5. 完成後複製網址

### Express server(Middleware-webhook)
1. .env-template copy to .env
2. 接收 SurveyCake 資料，收到的是加密資料
3. 發送 GoogleSheet webhook，帶上加密資料，會由App Script 那邊處理
4. 發送 super8 webhook，因為只需要通知，暫不需要內容，body傳什麼都可以

### How to use
1. pull 專案
    ```shell
    git clone
   ```
2. 安裝
    ```shell
    pnpm install
   ```
3. 本地測試
    ```shell
    pnpm run dev
   ```

### Deploy to Vercel
1. `mkdir api & cd api`
2. `touch index.js` (must be index.js, because vercel will auto find this file)
3. touch `.env` or `cp .env-template .env`
4. touch `vercel.json`
    ```json
   { "version": 2, "rewrites": [{ "source": "/(.*)", "destination": "/api" }] }
   ```
5. 安裝 vercel
    ``` shell
    pnpm i vercel
    ```
6. 登入 vercel
    ``` shell
    vercel login
    ```
7. local test vercel
    ``` shell
    vercel dev
    ```
8. 部署 vercel
    ``` shell
    vercel
    ```
9. 部署完成後，會有一個網址，將此網址填入 SurveyCake webhook url，如需修改 env 環境變數，需修改 .env 後再次部署，從 web 介面看不到完整 env 變數
10. 若已連結 git repository，可以透過指令將 vercel project 與 git repository 連結，git push 時會自動部署
    ``` shell
    vercel git connect
    ```

### How to test
1. 重新填寫問卷，即可看到資料存入 GoogleSheet
2. 可以看到 vercel(webhook-server) log 有收到資料
3. 也可以看到 GoogleSheet 有發送通知
4. 也可以看到 super8 有發送通知

### Reference
https://vercel.com/guides/using-express-with-vercel
https://github.com/SurveyCake/webhook/blob/master/README.md
https://blog.SurveyCake.com/features/pro/survey-google-sheets/
https://help.no8.io/zh-TW/articles/7000832-SurveyCake-%E5%95%8F%E5%8D%B7%E8%A1%A8%E5%96%AE